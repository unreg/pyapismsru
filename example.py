#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    pyapismsru.example

    Simple use sms.ru API.

    Part of PyAPIsmsru: Python-wrapper on API sms.ru

    :copyright: (c) 2013 unregistered <black@tmnhy.su>
    :license: MIT, see LICENSE for more details

    Typical use pattern:
    ./example.py -r 7xxxxxxxxxx -t "Hello" -k "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
"""

from smsruapi import SmsRuAPI
from optparse import OptionParser


optp = OptionParser()
optp.version = '%%prog 0.1'
optp.description = 'Some example program to use sms.ru API with simple authorization.'
optp.usage = '%prog [options]'

optp.add_option('-r', '--recipient', type = 'string', dest = 'phone',
    help = 'Recipient phone number.')
optp.add_option('-t', '--text', type = 'string', dest = 'text',
    help = 'Text message')
optp.add_option('-k', '--key', type = 'string', dest = 'api_id',
    help = 'sms.ru unique key.')

opts,args = optp.parse_args()

if opts.phone is None:
    opts.phone = raw_input("Recipient phone number: ")
if opts.text is None:
    opts.text = raw_input("Text message: ")
if opts.api_id is None:
    opts.api_id = raw_input("sms.ru api_id: ")

sms = SmsRuAPI(api_id = opts.api_id)
if sms.check():
    print sms.send(opts.phone, opts.text, translit = True)