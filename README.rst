=====================================
PyAPISmsRu: Python Implementation  API sms.ru
=====================================

Welcome to PyAPISmsRu!

PyAPISmsRu is both an implementation of the API sms-service http://sms.ru.

Nearly complete implementation: send sms, get sms-state by id, cost calculation, get account balance, get account limit, get senders.

Not implemented: send sms in multi-mode, manage the stoplist.


Реализация на Python API sms-сервиса http://sms.ru
========

Релизовано: отправка sms, проверка статуса отправленных sms, расчет стоимости и количества sms, получение баланса и лимита, получение списка пользователей.

Не реализовано: отправка sms в мульти-режиме, управление стоп-листом.


Usage
========

Typical use pattern::

    from smsruapi import SmsRuAPI
    sms = SmsRuAPI(api_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx')
    if sms.check():
        sms.send('+7960xxxxxxx', 'Съешь ещё этих мягких булочек.', translit = True)

A simple example - example.py::

    ./example.py -r 7960xxxxxxx -t "Text message" -k xxxxxxxx-xxxx-xxxx-xxxxxxxxxxxxxxxx

For example notice the login, part of /etc/profile::

    if [ -n "$SSH_CLIENT" ]; then
        TEXT="$(date +%Y-%m-%dT%H:%M:%S): ssh login to ${USER}@$(hostname -f)"
        TEXT="$TEXT from $(echo $SSH_CLIENT|awk '{print $1}')"
        /PATH_TO_EXAMPLE/example.py -r 7960xxxxxxx -t "$TEXT" -k xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    fi


Dependencies
========

Python 2.7 standart modules hashlib, logging, optparse and Requests - elegant and simple HTTP library for Python.


Feedback
========

Mail to: black@tmnhy.su