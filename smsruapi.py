# -*- coding:utf-8 -*-
"""
    pyapismsru.smsruapi

    This module provides API sms.ru functionality.

    Part of PyAPIsmsru: Python-wrapper on API sms.ru

    Depends on: http://docs.python-requests.org

    :copyright: (c) 2013 unregistered <black@tmnhy.su>
    :license: MIT, see LICENSE for more details
"""

import requests
import hashlib
import logging


log = logging.getLogger(__name__)

SMSRU_ANSWER_CODE = {
    '100': 'Сообщение принято к отправке',
    '200': 'Неправильный api_id',
    '201': 'Не хватает средств на лицевом счету',
    '202': 'Неправильно указан получатель',
    '203': 'Нет текста сообщения',
    '204': 'Имя отправителя не согласовано с администрацией',
    '205': 'Сообщение слишком длинное (превышает 8 СМС)',
    '206': 'Будет превышен или уже превышен дневной лимит на отправку сообщений',
    '207': 'На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей',
    '208': 'Параметр time указан неправильно',
    '209': 'Вы добавили этот номер (или один из номеров) в стоп-лист',
    '210': 'Используется GET, где необходимо использовать POST',
    '211': 'Метод не найден',
    '212': 'Текст сообщения необходимо передать в кодировке UTF-8 (вы передали в другой кодировке)',
    '220': 'Сервис временно недоступен, попробуйте чуть позже',
    '230': 'Сообщение не принято к отправке, так как на один номер в день нельзя отправлять более 250 сообщений',
    '300': 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
    '301': 'Неправильный пароль, либо пользователь не найден',
    '302': 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
}

SMSRU_STATUS_CODE = {
    '-1': 'Сообщение не найдено',
    '100': 'Сообщение находится в нашей очереди',
    '101': 'Сообщение передается оператору',
    '102': 'Сообщение отправлено (в пути)',
    '103': 'Сообщение доставлено',
    '104': 'Не может быть доставлено: время жизни истекло',
    '105': 'Не может быть доставлено: удалено оператором',
    '106': 'Не может быть доставлено: сбой в телефоне',
    '107': 'Не может быть доставлено: неизвестная причина',
    '108': 'Не может быть доставлено: отклонено',
    '200': 'Неправильный api_id',
    '210': 'Используется GET, где необходимо использовать POST',
    '211': 'Метод не найден',
    '220': 'Сервис временно недоступен, попробуйте чуть позже',
    '300': 'Неправильный token (возможно истек срок действия, либо ваш IP изменился)',
    '301': 'Неправильный пароль, либо пользователь не найден',
    '302': 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс)',
}

def a_descr(code, ):
    """
    Returns a description of the response code.
    """
    try:
        return SMSRU_ANSWER_CODE[code]
    except:
        log.error('(%s) - нет описания кода ответа', code)
        return 'Неизвестный код'

def s_descr(code, ):
    """
    Returns a description of the sms-status code.
    """
    try:
        return SMSRU_STATUS_CODE[code]
    except:
        log.error('(%s) - нет описания кода ответа', code)
        return 'Неизвестный код'


class SmsRuAPI(object):
    """
    The base class for access to the API sms.ru.

    Typical use pattern:

    .. code-block:: python

        from smsruapi import SmsRuAPI

        sms = SmsRuAPI(api_id = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx')
        sms.send('+7960xxxxxxx', 'Съешь ещё этих мягких булочек.', translit = True)

    :param login: Login for strong authorization.
    :param password: Password for strong authorization.
    :param api_id: Unique key for simple authorization.
    :param strong_auth: Selector a type authorization.
    """

    def __init__(self, login = None, password = None, 
                 api_id = None, strong_auth = False):
        """
        """
        self.url = 'http://sms.ru'
        self.api_id = api_id
        self.login = login
        self.password = password
        self.__authorize(strong_auth)

    def __authorize(self, strong_auth, ):
        """
        Set authorization method.

        :param strong_auth: Selector a type authorization.
        """
        self.__auth = {}
        if strong_auth:
            if not (self.login and self.password):
                log.error('Для усиленной авторизации необходимо указать login:password')
                return
            token = self.get_token()
            self.__auth = {
                'login': self.login,
                'token': token,
                'sha512': hashlib.sha512(''.join([self.password, token])).hexdigest()
            }
        else:
            if not self.api_id:
                log.error('Для обычной авторизации необходимо указать api_id')
                return
            self.__auth = {
                'api_id': self.api_id,
            }

    def request(self, uri, data, ):
        """
        """
        f_url = '/'.join([
                self.url,
                uri
            ])
        log.debug('POST: %s', f_url)
        r = requests.post(f_url, data = data)
        log.debug('HTTP code: %s', r.status_code)
        if r.status_code == requests.codes.ok:
            return r.text.encode('utf-8').split()

    def send(self, to, message, multi = '', from_p = None, time = None,
             translit = False, test = None, partner_id = None):
        """
        Sends one sms.

        :param to: Recipient phone number.
        :param message: Text message utf-8 encoding.
        :param multi:
        :param from_p: Sender name, default phone number.
        :param time: Time to send in UNIX TIME format.
        :param translit: Set transliteration.
        :param test: Set test mode.
        :param partner_id:partner_id.
        """
        data = {
            'to': to,
            'text': message,
            'multi': multi,
        }
        if test:
            data['test'] = test
        if translit:
            data['translit'] = translit
        if from_p:
            data['from'] = from_p
        data.update(self.__auth)
        answer = self.request('sms/send', data)
        if answer[0] == '100':
            log.debug('SMS code: (%s) %s %s', answer[0], answer[1],
                                              a_descr(answer[0]))
            return answer[1]
        else:
            log.debug('SMS code: (%s) %s', answer[0], a_descr(answer[0]))

    def status(self, sms_id, ):
        """
        Returns SMS status by sms_id.
        """
        data = {
            'id': sms_id,
        }
        data.update(self.__auth)
        answer = self.request('sms/status', data)
        if answer:
            log.debug('sms id=%s: (%s) %s', sms_id, answer[0], s_descr(answer[0]))
            return (answer[0], s_descr(answer[0]))

    def cost(self, to, message,):
        """
        Returns calculate the cost of sms.
        """
        data = {
            'to': to,
            'text': message,
        }
        data.update(self.__auth)
        answer = self.request('sms/cost', data)
        if answer[0] == '100':
            log.debug('SMS code: (%s) Сообщений %s на %s руб.', answer[0], answer[2],
                                                         answer[1])
            return (answer[1], answer[2])
        else:
            log.debug('SMS code: (%s) %s', answer[0], a_descr(answer[0]))

    def balance(self, ):
        """
        Returns the balance.
        """
        data = {}
        data.update(self.__auth)
        answer = self.request('my/balance', data)
        if answer[0] == '100':
            log.debug('SMS code: (%s) Баланс %s руб.', answer[0], answer[1])
            return answer[1]
        else:
            log.debug('SMS code: (%s) %s', answer[0], a_descr(answer[0]))

    def limit(self, ):
        """
        Returns the day limit of messages.
        """
        data = {}
        data.update(self.__auth)
        answer = self.request('my/limit', data)
        if answer[0] == '100':
            log.debug('SMS code: (%s) Отправлено %s из %s', answer[0], answer[2],
                                                           answer[1])
            return (answer[1], answer[2])
        else:
            log.debug('SMS code: (%s) %s', answer[0], a_descr(answer[0]))

    def senders(self, ):
        """
        Returns a list of senders.
        """
        data = {}
        data.update(self.__auth)
        answer = self.request('my/senders', data)
        if answer[0] == '100':
            log.debug('SMS code: (%s) Отправители - %s', answer[0], str(answer))
        else:
            log.debug('SMS code: (%s) %s', answer[0], a_descr(answer[0]))

    def check(self, ):
        """
        Checks the authorization.
        """
        data = {}
        data.update(self.__auth)
        answer = self.request('auth/check', data)
        if answer[0] == '100':
            log.debug('SMS code: (%s) Ok', answer[0])
            return True
        else:
            log.debug('SMS code: (%s) %s', answer[0], a_descr(answer[0]))

    def get_token(self, ):
        """
        Returns the token.
        """
        token = self.request('auth/get_token', data = None)[0]
        log.debug('token: %s', token)
        return token